<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DaysOff1JspSpring</title>

</head>
<body>
	this is my first jsp with Spring ${valueFromApp}

	<h1>My list of Employees:</h1>
	<table style="border: 1px solid;">
		<c:forEach items="${valueFromList}" var="employee">
			<tr>
				<td>Employee name: <c:out value="${employee.getName()}" /></td>
			</tr>
		</c:forEach>
	</table>

	
	<h1>list of employee after removed:</h1>
	<table style="border: 1px solid;">
		<c:forEach items="${valueFromList}" var="employee">
			<tr>
				<td>Employee name: <c:out value="${employee.getDepartmentName()}" /></td>

				<td>
					<form
						action="http://localhost:8080/Employee/employeeController/removeEmployee"
						, method="get">
						<input type="hidden" value="${employee.getFirstName()}" , name="firstName">
						<input type="submit" value="Delete">
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>




	<h1>Add Employees in data base using hibernate:</h1>
	<form
		action="http://localhost:8080/Employee/EmployeeController/addEmployeesHibernate"
		method=POST>
		<br> New employee's name:<input type="text" name="firstName"
			placeholder="name"> <br> New employee's departments:<input
			type="text" name="departmentName" placeholder="departmentName"> <br>
		New employee's manager Id :<input type="text" name="managerId"
			placeholder="managerId"> <br> <input type="submit"
			value="enter"></input>
	</form>

	<h1>list of Employee after removed from Hibernate:</h1>

	<td>
		<form
			action="http://localhost:8080/Employee/EmployeeController/removeEmployeeByName"
			, method="get">
			<input type="text" name="name" 	placeholder="name">
			<input type="submit" value="Delete">
		</form>
	</td>


<h1>list of Employees updated from Hibernate:</h1>

	<td>
		<form
			action="http://localhost:8080/EmployeeController/updateEmployeeByName"
			, method=POST>
			<input type="text" name="name" 	placeholder="name">
			<input type="submit" value="Update">
		</form>
	</td>

<a href="<c:url value="/EmployeeController/logout" />">Logout</a>


</body>
</html>

</body>
</html>