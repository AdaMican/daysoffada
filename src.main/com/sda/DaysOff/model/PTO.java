package com.sda.DaysOff.model;

public class PTO {
	
	private int ptoId;
	private int employeeId;
	private String firstName;
	private String lastName;
	private int managerId;
	private int departmentId;
	private int nrOfAvailableDays;
	private int nrOfRequestedDays;
	private int nrOfRemainingDays;
	private boolean aprovedPto;
	public PTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PTO(int employeeId, String firstName, String lastName, int managerId, int departmentId,
			int nrOfAvailableDays, int nrOfRequestedDays, int nrOfRemainingDays, boolean aprovedPto, int ptoId) {
		super();
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.managerId = managerId;
		this.departmentId = departmentId;
		this.nrOfAvailableDays = nrOfAvailableDays;
		this.nrOfRequestedDays = nrOfRequestedDays;
		this.nrOfRemainingDays = nrOfRemainingDays;
		this.aprovedPto = aprovedPto;
		this.ptoId = ptoId;
	}
	public PTO(int ptoId) {
		super();
		
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getManagerId() {
		return managerId;
	}
	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getNrOfAvailableDays() {
		return nrOfAvailableDays;
	}
	public void setNrOfAvailableDays(int nrOfAvailableDays) {
		this.nrOfAvailableDays = nrOfAvailableDays;
	}
	public int getNrOfRequestedDays() {
		return nrOfRequestedDays;
	}
	public void setNrOfRequestedDays(int nrOfRequestedDays) {
		this.nrOfRequestedDays = nrOfRequestedDays;
	}
	public int getNrOfRemainingDays() {
		return nrOfRemainingDays;
	}
	public void setNrOfRemainingDays(int nrOfRemainingDays) {
		this.nrOfRemainingDays = nrOfRemainingDays;
	}
	public boolean isAprovedPto() {
		return aprovedPto;
	}
	public void setAprovedPto(boolean aprovedPto) {
		this.aprovedPto = aprovedPto;
	}
	public int getPtoId() {
		return ptoId;
	}
	public void setPtoId(int ptoId) {
		this.ptoId = ptoId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (aprovedPto ? 1231 : 1237);
		result = prime * result + departmentId;
		result = prime * result + employeeId;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + managerId;
		result = prime * result + nrOfAvailableDays;
		result = prime * result + nrOfRemainingDays;
		result = prime * result + nrOfRequestedDays;
		result = prime * result + ptoId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PTO other = (PTO) obj;
		if (aprovedPto != other.aprovedPto)
			return false;
		if (departmentId != other.departmentId)
			return false;
		if (employeeId != other.employeeId)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (managerId != other.managerId)
			return false;
		if (nrOfAvailableDays != other.nrOfAvailableDays)
			return false;
		if (nrOfRemainingDays != other.nrOfRemainingDays)
			return false;
		if (nrOfRequestedDays != other.nrOfRequestedDays)
			return false;
		if (ptoId != other.ptoId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PTO [ptoId=" + ptoId + ", employeeId=" + employeeId + ", firstName=" + firstName + ", lastName="
				+ lastName + ", managerId=" + managerId + ", departmentId=" + departmentId + ", nrOfAvailableDays="
				+ nrOfAvailableDays + ", nrOfRequestedDays=" + nrOfRequestedDays + ", nrOfRemainingDays="
				+ nrOfRemainingDays + ", aprovedPto=" + aprovedPto + "]";
	}
	
}