package com.sda.DaysOff.service;

import java.util.List;

import com.sda.DaysOff.model.Department;
import com.sda.DaysOff.model.Employee;

public interface PositionService {
	
	public void addPosition(Employee e);
	public void removePosition(Employee e);
	public void updatePosition(Employee e);
	public List <Department> getAllPositionsByDepartmentId(); 
	
	

}
