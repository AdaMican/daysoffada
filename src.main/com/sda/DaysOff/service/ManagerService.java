package com.sda.DaysOff.service;

import java.util.List;

import com.sda.DaysOff.model.Manager;

public interface ManagerService {

	
	public void addNewManager(Manager m);
	public void removeManager(Manager m);
	public void updateManager(Manager m);
	public List <Manager> getAllManagers();
	
}
