package com.sda.DaysOff.service;

public interface PtoService {

	public void addNewPto();
	public void getAllPtoByEmployee();
	public void getAllPtoByManager();
	public void approvePto();
	
	
}
