package com.sda.DaysOff.service;

import java.util.List;

import com.sda.DaysOff.DAO.EmployeeDAO;
import com.sda.DaysOff.model.Employee;

public class EmployeeServiceDatabaseImpl implements EmployeeService {
	
	EmployeeDAO employeeDao;

	public void addEmployee(Employee e) {
		employeeDao.addEmployee(e);
		
	}

	public void removeEmployee(Employee e) {
		employeeDao.removeEmployee(e);
		
	}

	public List<Employee> getAllEmployee() {
		
		return employeeDao.getAllEmployee();
	}

	public void removeEmployeeByName(String name) {
		employeeDao.removeEmployeeByName(name);
	}

	public void updateEmployeeByName(String name) {
		employeeDao.updateEmployeeByName(name, name);
		
	}

}
