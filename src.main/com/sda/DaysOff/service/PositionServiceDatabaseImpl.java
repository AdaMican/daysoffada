package com.sda.DaysOff.service;

import java.util.List;

import com.sda.DaysOff.DAO.PositionDAO;
import com.sda.DaysOff.model.Department;
import com.sda.DaysOff.model.Employee;

public class PositionServiceDatabaseImpl implements PositionService {
	
	PositionDAO positionDao;

	public void addPosition(Employee e) {
		positionDao.addPosition(e);
		
	}

	public void removePosition(Employee e) {
		positionDao.removePosition(e);
		
	}

	public void updatePosition(Employee e) {
		positionDao.updatePosition(e);
		
	}

	public List<Department> getAllPositionsByDepartmentId() {
		
		return positionDao.getAllPositionsByDepartmentId();
	}
	
	

}
