package com.sda.DaysOff.service;

import java.util.List;

import com.sda.DaysOff.model.Employee;



public interface EmployeeService {


	public void addEmployee(Employee e);
	
	public void removeEmployee(Employee e);
	
	public List<Employee> getAllEmployee();

	public void removeEmployeeByName(String name);
	
	public void updateEmployeeByName(String name);
}
