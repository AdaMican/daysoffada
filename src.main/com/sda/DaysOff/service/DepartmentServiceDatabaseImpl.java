package com.sda.DaysOff.service;

import java.util.List;

import com.sda.DaysOff.DAO.DepartmentDAO;
import com.sda.DaysOff.model.Employee;
import com.sda.DaysOff.model.Manager;

public class DepartmentServiceDatabaseImpl implements DepartmentService{
	
	DepartmentDAO departmentDao;

	public List<Employee> getAllEmployeeByDepartment() {
		
		return departmentDao.getAllEmployeeByDepartment();
	}

	public List<Manager> getAllManagersByDepartment() {
		return departmentDao.getAllManagersByDepartment();
		
	}

	public void updateDepartment(Employee e) {
		departmentDao.updateDepartment(e);
		
	}

}
