package com.sda.DaysOff.service;

import java.util.List;

import com.sda.DaysOff.DAO.ManagerDAO;
import com.sda.DaysOff.model.Manager;

public class ManagerServiceDatabaseImpl implements ManagerService{
	
	ManagerDAO managerDao;

	public void addNewManager(Manager m) {
		managerDao.addNewManager(m);
		
	}

	public void removeManager(Manager m) {
		managerDao.removeManager(m);
		
	}

	public void updateManager(Manager m) {
		managerDao.updateManager(m);
		
	}

	public List<Manager> getAllManagers() {
		
		return managerDao.getAllManagers();
	}

}
