package com.sda.DaysOff.service;

import com.sda.DaysOff.DAO.PtoDAO;

public class PtoServiceDatabaseImpl implements PtoService {
	
	PtoDAO ptoDao;

	public void addNewPto() {
		ptoDao.addNewPto();
		
	}

	public void getAllPtoByEmployee() {
		ptoDao.getAllPtoByEmployee();
		
	}

	public void getAllPtoByManager() {
		ptoDao.getAllPtoByManager();
		
	}

	public void approvePto() {
		ptoDao.approvePto();
		
	}

}
