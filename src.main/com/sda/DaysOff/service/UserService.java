package com.sda.DaysOff.service;

import java.util.List;

import com.sda.DaysOff.model.User;



public interface UserService {
	
	public void addUser(User u);

	public void removeUser(User u);

	public List<User> getAllUsers();
	
public void removeUserByName(String userName);
	
	public void updateUserByName(String userName);

	public User getUserByUsername(String userName);

}
