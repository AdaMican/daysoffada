package com.sda.DaysOff.DAO;

import java.util.List;

import com.sda.DaysOff.model.Employee;
import com.sda.DaysOff.model.Manager;

public interface DepartmentDAO {
	
	public List<Employee> getAllEmployeeByDepartment();
	public List<Manager> getAllManagersByDepartment();
	public void updateDepartment (Employee e);
	

}
