package com.sda.DaysOff.DAO;

public interface PtoDAO {

	public void addNewPto();
	public void getAllPtoByEmployee();
	public void getAllPtoByManager();
	public void approvePto();
	
	
}
