package com.sda.DaysOff.DAO;

import java.util.List;

import com.sda.DaysOff.model.User;



public interface UserDAO {
	
	public User getUserByUsername(String userName);
	
public void addUser(User u);
	
	public void removeUser(User u);
	
	public List<User> getAllUsers();

	public void removeUserByName(String userName);
	
	public void updateUserByName(String userName);

}
