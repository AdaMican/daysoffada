package com.sda.DaysOff.DAO;

import java.util.List;

import com.sda.DaysOff.model.Manager;

public interface ManagerDAO {

	
	public void addNewManager(Manager m);
	public void removeManager(Manager m);
	public void updateManager(Manager m);
	public List <Manager> getAllManagers();
	
}
