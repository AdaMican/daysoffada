package com.sda.DaysOff.DAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.sda.DaysOff.model.Employee;


public class EmployeeDAOHibernateImpl implements EmployeeDAO{
	
	private SessionFactory sesionFactory;
	
	

	public SessionFactory getSesionFactory() {
		return sesionFactory;
	}

	public void setSesionFactory(SessionFactory sesionFactory) {
		this.sesionFactory = sesionFactory;
	}

	public void addEmployee(Employee e) {
		Session sesion = sesionFactory.openSession();
		Transaction transaction = null;
		
		try {
			transaction = sesion.beginTransaction();
			sesion.save(e);
			transaction.commit();
			sesion.close();
		} catch (HibernateException exc) {
			if (transaction != null) {
				transaction.rollback();
			}
			sesion.close();
			System.out.println("new employee was added");
		}
	}
		
	

	public void removeEmployee(Employee e) {
		Session sesion = sesionFactory.openSession();
		Transaction transaction = null;
		
		try {
			transaction = sesion.beginTransaction();
			sesion.remove(e);
			transaction.commit();
			sesion.close();
		} catch (HibernateException exc) {
			if (transaction != null) {
				transaction.rollback();
			}
			sesion.close();
			System.out.println("Employee: "+e+" was removed");
		}
		
	}

	public List<Employee> getAllEmployee() {
		Session sesion = sesionFactory.openSession();
		Transaction transaction = sesion.beginTransaction();
		List<Employee> employees = sesion.createQuery("from Employee").list();

		for (Employee e : employees) {
			System.out.println(e);
		}
		transaction.commit();
		sesion.close();
			return employees;
	}
	
		

	public void removeEmployeeByName(String name) {
		// TODO Auto-generated method stub
		
	}

	public void updateEmployeeByName(String oldName, String newName) {
	
		Session sesion = sesionFactory.openSession();
		Transaction transaction = null;
		
		try {
			transaction = sesion.beginTransaction();
			String hql = "update Employee set newName= :name where oldName = :name";
            Query q = sesion.createQuery(hql).setParameter("name", newName);

			transaction.commit();
			sesion.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			sesion.close();
		}
		
	
		
	}

}
